extends EditorInspectorPlugin

const InspectorScene = preload("res://addons/time_series_plot/scenes/inspector.tscn")

func _can_handle(object) -> bool:
	# We support all objects in this example.
	if object is TimeSeriesPlot:
		return true
	return false

func _parse_begin( object ):
	var tsp := object as TimeSeriesPlot

	var inspector = InspectorScene.instantiate()
	inspector.set_values( tsp )
	inspector.connect("enable_fake_data",
		func( enabled ):
			if enabled: tsp._fake_continuous_start()
			else: tsp._fake_continuous_stop()
	)
	inspector.connect("fake_min_changed",
		func( value ):
			tsp.fake_data_range_start = value
			tsp._fake_continuous_update()
	)
	inspector.connect("fake_max_changed",
		func( value ):
			tsp.fake_data_range_end = value
			tsp._fake_continuous_update()
	)
	inspector.connect("fake_deviation_changed",
		func( value ):
			tsp.fake_cont_deviation = value
			tsp._fake_continuous_update()
	)
	add_custom_control(inspector)
