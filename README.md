# godot-tsp - TimeSeriesPlot extends Control

## Description
I just wanted a graph for tracking data over time, I tried [Easy Charts](https://github.com/fenix-hub/godot-engine.easy-charts) but it didnt appear to show a preview in the editor of what I would see.

## Installation
Clone the repo into a new folder in your addons directory called 'godot_tsp'

Your project should look like:

\<your project root\>/addons/godot_tsp/\<contents of this git repo\>

Then enable it in the project settings / plugins section

## Usage
This plugin adds a new TimeSeriesPlot node, add it to your scene, and push data to it using `$mynode.push({'mykey':data})`.
