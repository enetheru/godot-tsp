@tool
class_name TspTheme extends Resource

signal cosmetic_change
signal layout_change

@export_group("Plot", "plot_")
# TODO This requires a new polyline implementation to be written for the engine itself.
#@export var plot_line_thickness : float = 3.0:
#	set(value): plot_line_thickness = value; cosmetic_change.emit()
@export var plot_background : Color = Color.SLATE_GRAY:
	set(value): plot_background = value; cosmetic_change.emit()

@export_group("Margins", "margin_" )
@export var margin_background : Color = Color.DIM_GRAY :
	set(value): margin_background = value; cosmetic_change.emit()
@export_range(0,8191) var margin_top : int = 100:
	set(value): margin_top = value; layout_change.emit()
@export_range(0,8191) var margin_left : int = 50:
	set(value): margin_left = value; layout_change.emit()
@export_range(0,8191) var margin_right : int = 50:
	set(value): margin_right = value; layout_change.emit()
@export_range(0,8191) var margin_bottom : int = 50 :
	set(value): margin_bottom = value; layout_change.emit()

@export_group("Title", "title_")
@export var title_enabled : bool = true :
	set(value): title_enabled = value; layout_change.emit()
@export var title_font : String = "Not Implemented" : #ThemeDB.fallback_font
	set(value):title_font = "Not Implemented"; cosmetic_change.emit()
@export_range(6,72) var title_size : int = 20 :
	set(value): title_size = value; layout_change.emit()
@export var title_location : VerticalAlignment = VERTICAL_ALIGNMENT_TOP :
	set(value): title_location = value; layout_change.emit()
@export var title_color : Color = Color.WHITE :
	set(value): title_color = value; cosmetic_change.emit()

@export_group("Legend", "legend_")
@export var legend_enabled : bool = true :
	set(value): legend_enabled = value; layout_change.emit()
@export var legend_size : int = 16 :
	set(value): legend_size = value; layout_change.emit()
@export var legend_color : Color = Color.DIM_GRAY :
	set(value): legend_color = value; layout_change.emit()

@export_group("Division Major", "div_major_")
@export var div_major_line_width : float = 2.0 :
	set(value): div_major_line_width = value; cosmetic_change.emit()
@export var div_major_line_color : Color = Color.WHITE :
	set(value): div_major_line_color = value; cosmetic_change.emit()
@export var div_major_tick_length : float = 10.0 :
	set(value): div_major_tick_length = value; cosmetic_change.emit()
@export var div_major_tick_width : float = 4.0 :
	set(value): div_major_tick_width = value; cosmetic_change.emit()
@export var div_major_tick_color : Color = Color.WHITE :
	set(value): div_major_tick_color = value; cosmetic_change.emit()
@export var div_major_label_size : int = 16 :
	set(value): div_major_label_size = value; cosmetic_change.emit()
@export var div_major_label_color : Color = Color.WHITE :
	set(value): div_major_label_color = value; cosmetic_change.emit()
@export var div_major_label_font : String = "Not Implemented" : #ThemeDB.fallback_font
	set(value): div_major_label_font = value; cosmetic_change.emit()

@export_group("Division Minor", "div_minor_")
@export var div_minor_line_width : float = 2.0 :
	set(value): div_minor_line_width = value; cosmetic_change.emit()
@export var div_minor_line_color : Color = Color.WHITE :
	set(value): div_minor_line_color = value; cosmetic_change.emit()
@export var div_minor_tick_length : float = 10.0 :
	set(value): div_minor_tick_length = value; cosmetic_change.emit()
@export var div_minor_tick_width : float = 4.0 :
	set(value): div_minor_tick_width = value; cosmetic_change.emit()
@export var div_minor_tick_color : Color = Color.WHITE :
	set(value): div_minor_tick_color = value; cosmetic_change.emit()
@export var div_minor_label_size : int = 16 :
	set(value): div_minor_label_size = value; cosmetic_change.emit()
@export var div_minor_label_color : Color = Color .WHITE :
	set(value): div_minor_label_color = value; cosmetic_change.emit()
@export var div_minor_label_font : String = "Not Implemented" : #ThemeDB.fallback_font
	set(value): div_minor_label_font = value; cosmetic_change.emit()

@export_group("X-Axis( Domain )", "xaxis_")
# X-Axis Title
@export_subgroup("Title", "xaxis_title_")
@export var xaxis_title_color: Color = Color.WHITE :
	set(value): xaxis_title_color = value; cosmetic_change.emit()
@export var xaxis_title_size: int = 20 :
	set(value): xaxis_title_size = value; cosmetic_change.emit()

@export_subgroup("Lines", "xaxis_")
@export var xaxis_lines_enabled : bool = true :
		set(value): xaxis_lines_enabled = value; layout_change.emit()

# X-Axis Ticks
@export_subgroup("Ticks", "xaxis_")
@export var xaxis_ticks_enabled : bool = true :
		set(value): xaxis_ticks_enabled = value; layout_change.emit()
@export var xaxis_major_every : int = 1000000 :
	set(value): xaxis_major_every = value; layout_change.emit()
@export var xaxis_minor_every : int = 2 :
	set(value): xaxis_minor_every = value; layout_change.emit()

# Y-Axis or Range
# ===============
@export_group("Y-Axis( Range )", "yaxis_")
# Title
@export_subgroup("Title", "yaxis_title_")
@export var yaxis_title_color: Color = Color.WHITE :
	set(value): yaxis_title_color = value; cosmetic_change.emit()
@export var yaxis_title_size: int = 20 :
	set(value): yaxis_title_size = value; cosmetic_change.emit()
# Lines
@export_subgroup("Lines", "ylines_")
@export var ylines_show_zero : bool = true :
	set(value): ylines_show_zero = value; layout_change.emit()
@export var ylines_show_divisions : bool = true :
	set(value): ylines_show_divisions = value; layout_change.emit()

# Ticks
@export_subgroup("Ticks", "yticks_")
@export var yticks_show_extents : bool = true :
	set(value): yticks_show_extents = value; layout_change.emit()
@export var yticks_show_divisions : bool = true :
	set(value): yticks_show_divisions = value; layout_change.emit()
@export var yticks_show_zero : bool = true :
	set(value): yticks_show_zero = value; layout_change.emit()

@export_subgroup("Divisions", "ydiv_")
@export var ydiv_major_every : int = 10 :
	set(value): ydiv_major_every = value; layout_change.emit()
@export var ydiv_minor_every : int = 2 :
	set(value): ydiv_minor_every = value; layout_change.emit()

# Plot Borders
# ============
@export_group("Border", "border_")
@export var border_color : Color = Color.WHITE :
	set(value): border_color = value; cosmetic_change.emit()
@export var border_thickness : float = 3.0 :
	set(value): border_thickness = value; cosmetic_change.emit()
@export var border_north_enabled : bool = false :
	set(value): border_north_enabled = value; cosmetic_change.emit()
@export var border_south_enabled : bool = false :
	set(value): border_south_enabled = value; cosmetic_change.emit()
@export var border_east_enabled : bool = false :
	set(value): border_east_enabled = value; cosmetic_change.emit()
@export var border_west_enabled : bool = false :
	set(value): border_west_enabled = value; cosmetic_change.emit()
