# plugin.gd
@tool
extends EditorPlugin

var inspector_plugin = preload("res://addons/time_series_plot/inspector.gd")
var type_plugin = preload("res://addons/time_series_plot/tsp.gd")
var type_icon = preload("res://addons/time_series_plot/icon.svg")


func _enter_tree():
	inspector_plugin = inspector_plugin.new()
	add_inspector_plugin(inspector_plugin)
	add_custom_type("TimeSeriesPlot", "Control", type_plugin, type_icon )


func _exit_tree():
	remove_inspector_plugin(inspector_plugin)
	remove_custom_type("TimeSeriesPlot")
