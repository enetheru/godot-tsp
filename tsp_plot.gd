@tool
class_name TspPlot extends Control

var recalc_flag : bool = false;

var tsp : TimeSeriesPlot

var centre_offset := Vector2(0,0)

var plots : Dictionary

var div_major_values : PackedInt32Array
var div_major_labels : PackedStringArray
var div_major_lines : PackedVector2Array

var div_minor_values : PackedInt32Array
var div_minor_labels : PackedStringArray
var div_minor_lines : PackedVector2Array

@onready var default_font := ThemeDB.fallback_font

func queue_recalc():
	recalc_flag = true
	queue_redraw()

func recalc_all():
	if not tsp: return
	calc_multiplot( tsp._data )
	calc_major_div()
	centre_offset.y = 1.0 #zero line at bottom of rect
	centre_offset.y += tsp.range_start / tsp.range_width

func calc_multiplot( data : Array[Dictionary] ):
	for key in tsp.legend.keys():
		if not plots.has(key):
			var points : PackedVector2Array
			points.resize( data.size() )
			plots[key] = points
		plots[key].resize( data.size() )

	for i in range( data.size() ):
		# error checking for dict without 't'
		var last_t = 0
		if data[i].has('t'): last_t = data[i]['t']
		else : data[i]['t'] = last_t
		var xn = (data[i]["t"] - tsp.domain_start) as float / tsp.domain_width

		for key in tsp.legend.keys():
			if not data[i].has(key): continue
			if not data[i][key] is float: continue

			var yn = data[i][key] / tsp.range_width

			plots[key][i].x = xn * size.x + centre_offset.x * size.x
			plots[key][i].y = -yn * size.y + centre_offset.y * size.y

func calc_major_div():
	#FIXME, these coordinate pairs only need to be calculated sometimes.
	# it strikes me that they might be so predictable that all the calculation is completely unnecessary
	div_major_labels.clear()
	div_major_values.clear()
	div_major_lines.clear()

	var style = tsp.style

	if style.xaxis_major_every < 1: return #nothing to do

	var ndivisions = tsp.domain_width / style.xaxis_major_every
	if ndivisions > 100: return # skip doing dumb things.
	ndivisions += 5
	div_major_labels.resize(ndivisions)
	div_major_values.resize(ndivisions)
	div_major_lines.resize(ndivisions * 2)

	# generate positions based on tsp domain
	var start : int
	start = tsp.domain_start - (tsp.domain_start % style.xaxis_major_every)
	start += style.xaxis_major_every

	for i in range( ndivisions ):
		var value : int = start + i * style.xaxis_major_every
		div_major_values[i] = value
		div_major_labels[i] = tsp.domain_to_string_relative( value )
		var norm : float = (value - tsp.domain_start) as float / tsp.domain_width
		var t := Vector2( norm * tsp.plot_width, 0)
		var b := t + Vector2(0, tsp.plot_height )
		div_major_lines[i * 2] = t
		div_major_lines[i * 2 + 1] = b

func _draw():
	if not tsp: return
	var style = tsp.style
	if recalc_flag:
		recalc_flag = false
		recalc_all()

	if style.xaxis_lines_enabled and div_major_values.size() > 0:
		draw_multiline( div_major_lines, style.div_major_line_color, style.div_major_line_width )

	if style.xaxis_ticks_enabled: _draw_xaxis()

	# Plot data
	for key in tsp.legend.keys():
		if plots.has(key) and plots[key].size() > 1:
			draw_polyline( plots[key], tsp.legend[key] )

func _draw_xaxis():
	var style = tsp.style

	if style.margin_bottom < style.div_major_tick_length + style.div_major_label_size : return
	for i in range( 0, div_major_values.size() ):
		_draw_tick( div_major_lines[i * 2 + 1], "BELOW",
			style.div_major_tick_length, style.div_major_tick_width, style.div_major_tick_color,
			div_major_labels[i], style.div_major_label_color, style.div_major_label_size )

func _draw_tick( position :Vector2, alignment, tick_length, tick_width, tick_color, label_text, label_color, label_size, label_font = "" ):
	var tick_end : Vector2
	var label_start : Vector2
	var label_align
	var label_width = label_text.length() * label_size
	var label_half = label_width / 2
	match alignment:
		"ABOVE":
			tick_end = position + Vector2(0,-1) * tick_length
			label_align = HORIZONTAL_ALIGNMENT_CENTER
			label_start = tick_end + Vector2( -label_half, -3 )
		"BELOW":
			tick_end = position + Vector2(0,1) * tick_length
			label_align = HORIZONTAL_ALIGNMENT_CENTER
			label_start = tick_end + Vector2( -label_half, label_size + 3)
		"LEFTOF":
			tick_end = position + Vector2(-1,0) * tick_length
			label_align = HORIZONTAL_ALIGNMENT_RIGHT
			label_start = tick_end + Vector2( -label_width - 3, label_size / 3)
		"RIGHTOF":
			tick_end = position + Vector2(1,0) * tick_length
			label_align = HORIZONTAL_ALIGNMENT_LEFT
			label_start = tick_end + Vector2( 3, label_size / 3)

	draw_line(position, tick_end, tick_color, tick_width )

	draw_string(
		default_font,
		label_start,
		label_text,
		label_align,
		label_width,
		label_size,
		label_color )

# Callbacks
func _on_resized():
	queue_recalc()
