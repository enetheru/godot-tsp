@tool
extends VBoxContainer

signal enable_fake_data( enabled )
signal fake_min_changed( value )
signal fake_max_changed( value )
signal fake_deviation_changed( value )

func _on_fake_data_toggled(toggled_on):
	enable_fake_data.emit(toggled_on)

func set_values( tsp : TimeSeriesPlot ):
	$CheckButton.button_pressed = tsp.fake_cont_enabled
	$fake_min/SpinBox.value = tsp.fake_data_range_start
	$fake_max/SpinBox.value = tsp.fake_data_range_end
	$fake_deviation/SpinBox.value = tsp.fake_cont_deviation

func _on_fake_min_value_changed(value):
	fake_min_changed.emit( value )

func _on_fake_max_value_changed(value):
	fake_max_changed.emit( value )

func _on_fake_deviation_value_changed(value):
	fake_deviation_changed.emit( value )
